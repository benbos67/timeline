package au.test.timeline;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ObjectFactory {

	public static void setObjectPropertiesToNull(Object o) {

		Class<? extends Object> clazz = o.getClass();
		Arrays.asList(clazz.getMethods()).forEach(method -> {
			if (Modifier.isPublic(method.getModifiers()) && method.getName().startsWith("set")
					&& method.getParameterCount() == 1 && !method.getParameterTypes()[0].isPrimitive()) {
				try {
					method.invoke(o, (Object) null);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Arrays.asList(clazz.getDeclaredFields()).forEach(field -> {
			if (Modifier.isPublic(field.getModifiers()) && !field.getType().isPrimitive()) {
				try {
					field.set(o, (Object) null);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

}
