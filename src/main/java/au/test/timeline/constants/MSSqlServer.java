package au.test.timeline.constants;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MSSqlServer {
	
	public static final LocalDate MS_SQL_MAX_DATE = LocalDate.of(9999, 12, 31);
	
	public static final LocalDate MS_SQL_MIN_DATE = LocalDate.of(1753, 1, 1);
	
	public static final LocalDateTime MS_SQL_MAX_DATE_TIME = LocalDateTime.of(1753, 12, 31, 23, 59, 59, 999999999);

	public static final LocalDateTime MS_SQL_MIN_DATE_TIME = LocalDateTime.of(1753, 1, 1, 0, 0, 0, 0);

}
