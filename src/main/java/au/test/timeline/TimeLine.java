package au.test.timeline;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public final class TimeLine<T extends TimeFrame> extends ArrayList<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4383011627555715876L;

	public TimeLine() {
	}

	public TimeLine(Collection<T> source) {
		this.addAll(source);
	}

	/**
	 * Merges Timeframables based on a predicate
	 * 
	 * @param predicate
	 */
	public void fuse(BiPredicate<T, T> predicate) {
		TimeLine<T> fused = new TimeLine<>();
		Iterator<T> iterator = this.iterator();
		if (iterator.hasNext()) {
			T base = (T) iterator.next().clone();
			if (iterator.hasNext()) {
				while (iterator.hasNext()) {
					T prospect = (T) iterator.next();
					if (predicate.test(base, prospect)) {
						base.setEndDate(prospect.getEndDate());
					} else {
						fused.add(base);
						base = (T) prospect.clone();
					}
				}
			}
			fused.add(base);
		}
		this.clear();
		this.addAll(fused);
	}

	/**
	 * Spits a Timeline at a given date
	 * 
	 * @param date
	 */
	public void split(LocalDate date) {
		TimeLine<T> temporaryTimeLine = ceiling(date);
		temporaryTimeLine.addAll(floor(date));
		this.clear();
		this.addAll(temporaryTimeLine);
	}

	/**
	 * Cap a Timeline at a ceiling date
	 * 
	 * @param date
	 * @return
	 */
	public TimeLine<T> ceiling(LocalDate date) {
		return new TimeLine<>(this.stream().map(t -> {
			if (t.contains(date) && !date.equals(t.getStartDate())) {
				T clone = (T) t.clone();
				clone.setEndDate(date);
				return clone;
			}
			if (t.getEndDate().isAfter(date)) {
				return null;
			}
			return (T) t.clone();
		}).filter(t -> t != null).collect(Collectors.toList()));
	}

	/**
	 * Cap a Timeline at a floor date
	 * 
	 * @param date
	 * @return
	 */
	public TimeLine<T> floor(LocalDate date) {
		return new TimeLine<>(this.stream().map(t -> {
			if (t.contains(date) && !date.equals(t.getEndDate())) {
				T clone = (T) t.clone();
				clone.setStartDate(date);
				return clone;
			}
			if (t.getStartDate().isBefore(date)) {
				return null;
			}
			return (T) t.clone();
		}).filter(t -> t != null).collect(Collectors.toList()));
	}

	/**
	 * Returns a Timeline populated with Timeframables which cover non-covered
	 * periods
	 * 
	 * @return
	 */
	public TimeLine<BasicTimeFrame> getUnCovered() {
		TimeLine<BasicTimeFrame> result = new TimeLine<>();
		Iterator<T> iterator = this.iterator();
		if (iterator.hasNext()) {
			T t = (T) iterator.next();
			LocalDate date = t.getEndDate();
			while (iterator.hasNext()) {
				t = (T) iterator.next();
				if (date.isBefore(t.getStartDate())) {
					result.add(new BasicTimeFrame(date, t.getStartDate()));
				}
				date = t.getEndDate();
			}
		}

		return result;
	}

}
