package au.test.timeline;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.Temporal;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.test.timeline.constants.MSSqlServer;

public abstract class TimeFrame implements Comparable<TimeFramable>, TimeFramable {

	private LocalDate startDate = LocalDate.now();
	@JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = MSSqlMaxDateFilter.class)
	private LocalDate endDate = MSSqlServer.MS_SQL_MAX_DATE;
	private Duration duration;

	@Override
	public LocalDate getStartDate() {
		return startDate;
	}

	@Override
	public abstract TimeFramable clone();

	public TimeFrame() {
		setDuration();
	}

	public TimeFrame(LocalDate startDate, LocalDate endDate) {
		if (startDate != null)
			setStartDate(startDate);
		if (endDate != null)
			setEndDate(endDate);
	}

	public TimeFrame(TimeFramable source) {
		setStartDate(source.getStartDate());
		setEndDate(source.getEndDate());

	}

	private LocalDate limit(LocalDate date) {
		if (date.isAfter(MSSqlServer.MS_SQL_MAX_DATE))
			return MSSqlServer.MS_SQL_MAX_DATE;
		if (date.isBefore(MSSqlServer.MS_SQL_MIN_DATE))
			return MSSqlServer.MS_SQL_MIN_DATE;
		return date;
	}

	@Override
	public void setStartDate(LocalDate startDate) {
		if (startDate == null)
			return;
		this.startDate = limit(startDate);
		if (this.startDate.isAfter(this.endDate))
			this.endDate = this.startDate;
		setDuration();
	}

	@Override
	public LocalDate getEndDate() {
		return endDate;
	}

	@Override
	public void setEndDate(LocalDate endDate) {
		if (endDate == null)
			return;
		this.endDate = limit(endDate);
		if (this.endDate.isBefore(this.startDate))
			this.startDate = this.endDate;
		setDuration();
	}

	@Override
	public Duration getDuration() {
		return this.duration;
	}

	public void setDuration() {
		this.duration = Duration.between(LocalDateTime.of(startDate, LocalTime.MIN), LocalDateTime.of(endDate, LocalTime.MIN));
	}

	public boolean doesIntersectWith(TimeFramable other) {
		if (other == null) {
			return false;
		}
		return (this.startDate.isBefore(other.getEndDate()) && this.endDate.isAfter(other.getStartDate()));
	}

	public Optional<TimeFramable> intersection(TimeFramable other) {
		if (this.doesIntersectWith(other)) {
			LocalDate start = other.getStartDate();
			LocalDate end = this.endDate;
			if (this.startDate.isAfter(other.getStartDate())) {
				start = this.startDate;
			}
			if (this.endDate.isAfter(other.getEndDate())) {
				end = other.getEndDate();
			}
			return Optional.of(new BasicTimeFrame(start, end));
		}
		return Optional.empty();
	}

	@Override
	public String toString() {
		return "TimeFrame [startDate=" + startDate + ", endDate=" + endDate + "]";
	}

	@Override
	public int compareTo(TimeFramable other) {
		if (other == null) {
			return 1;
		}
		if (this.startDate.isBefore(other.getStartDate())) {
			return -1;
		}
		if (this.startDate.isAfter(other.getStartDate())) {
			return 1;
		}
		return this.duration.compareTo(other.getDuration());
	}

	@Override
	public boolean contains(LocalDate date) {
		if (this.startDate.isAfter(date))
			return false;
		if (this.endDate.isBefore(date))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeFrame other = (TimeFrame) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

}
