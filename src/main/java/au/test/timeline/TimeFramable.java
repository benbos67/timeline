package au.test.timeline;

import java.time.Duration;
import java.time.LocalDate;

public interface TimeFramable {
	
	public void setStartDate(LocalDate start);
	
	public void setEndDate(LocalDate end);

	public LocalDate getStartDate();
	
	public LocalDate getEndDate();
	
	public Duration getDuration();
	
	public boolean contains(LocalDate date);

	int compareTo(TimeFramable other);
	
	TimeFramable clone();
	
}
