package au.test.timeline;

import java.time.LocalDate;

public class BasicTimeFrame extends TimeFrame {
	
	public BasicTimeFrame() {
	}

	public BasicTimeFrame(LocalDate start, LocalDate end) {
		super(start, end);
	}
	
	public BasicTimeFrame(BasicTimeFrame source) {
		super(source.getStartDate(), source.getEndDate());
	}

	@Override
	public BasicTimeFrame clone() {
		return new BasicTimeFrame(this);
	}

}
