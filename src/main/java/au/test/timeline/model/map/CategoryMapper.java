package au.test.timeline.model.map;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import au.test.timeline.entity.CategoryHistory;
import au.test.timeline.enums.MemberStatus;
import au.test.timeline.model.dto.CategoryDto;

@Mapper(componentModel = "spring")
public abstract class CategoryMapper {

	public static CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

	@Mapping(source = "source.newCategory", target = "category")
	@Mapping(source = "source.effectiveDate", target = "startDate")
	@Mapping(target = "status", source = "source")
	public abstract CategoryDto categoryHistoryToCategoryDto(CategoryHistory source);

	public MemberStatus stringToMemberStatus(CategoryHistory source) {
		String status = source.getStatus();
		MemberStatus[] memberStatuses = MemberStatus.values();
		for (MemberStatus m : memberStatuses) {
			if (m.getCode().equals(status))
				return m;
		}
		return null;
	}

}