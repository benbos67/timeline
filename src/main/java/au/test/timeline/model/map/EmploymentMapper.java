package au.test.timeline.model.map;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import au.test.timeline.entity.PayrollHistory;
import au.test.timeline.enums.EmploymentType;
import au.test.timeline.model.dto.EmploymentDto;

@Mapper(componentModel = "spring")
public abstract class EmploymentMapper {
	
	public static EmploymentMapper INSTANCE = Mappers.getMapper(EmploymentMapper.class);
	
	@Mapping(source = "source.effectiveDate", target = "startDate")
	@Mapping(source = "source", target = "typeOfEmployment")
	public abstract EmploymentDto payrollToEmploymentDto(PayrollHistory source);
	
	public EmploymentType stringToEmploymentType(PayrollHistory source) {
			String employmentType = source.getEmploymentType();
			EmploymentType[] employmentTypes = EmploymentType.values();
			for (EmploymentType e : employmentTypes) {
				if (e.getCode().equals(employmentType))
					return e;
			}
			return null;

	}

}