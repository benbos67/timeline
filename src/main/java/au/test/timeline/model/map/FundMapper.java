package au.test.timeline.model.map;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import au.test.timeline.entity.Fund;
import au.test.timeline.model.dto.FundDto;

@Mapper(componentModel = "spring")
public interface FundMapper {
	
	FundMapper INSTANCE = Mappers.getMapper(FundMapper.class);
	
	FundDto fundToFundDto(Fund fund);

}
