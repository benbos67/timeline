package au.test.timeline.model.map;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import au.test.timeline.entity.Member;
import au.test.timeline.model.dto.AccountDto;

@Mapper(uses = {FundMapper.class}, componentModel = "spring")
public interface AccountMapper {
	
	AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);
	
	@Mapping(source = "joinedDate", target = "startDate")
	@Mapping(source = "exitDate", target = "endDate")
	@Mapping(source = "memberNumber", target = "accountID")
	AccountDto memberToAccountDto(Member member);

}
