package au.test.timeline.model.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class FundDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5737572880239088220L;
	
	private String fundCode;
	private String division;
	private LocalDate lastReviewDate;

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public LocalDate getLastReviewDate() {
		return lastReviewDate;
	}

	public void setLastReviewDate(LocalDate lastReviewDate) {
		this.lastReviewDate = lastReviewDate;
	}

}
