package au.test.timeline.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import au.test.timeline.TimeFramable;
import au.test.timeline.TimeFrame;
import au.test.timeline.enums.EmploymentType;
import au.test.timeline.enums.WorkingStatus;

public class EmploymentDto extends TimeFrame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3244602907485227462L;

	private String employerCode;
	@JsonInclude(Include.NON_NULL)
	private EmploymentType typeOfEmployment;
	@JsonInclude(Include.NON_NULL)
	private WorkingStatus workingStatus;

	public EmploymentDto() {
	}
	
	public EmploymentDto(TimeFramable source) {
		super(source.getStartDate(), source.getEndDate());
	}
	
	public EmploymentDto(EmploymentDto source) {
		super(source.getStartDate(), source.getEndDate());
		this.employerCode = source.getEmployerCode();
		this.typeOfEmployment = source.getTypeOfEmployment();
		this.workingStatus = source.getWorkingStatus();
	}

	public String getEmployerCode() {
		return employerCode;
	}

	public void setEmployerCode(String employerCode) {
		this.employerCode = employerCode;
	}

	public EmploymentType getTypeOfEmployment() {
		return typeOfEmployment;
	}

	public void setTypeOfEmployment(EmploymentType typeOfEmployment) {
		this.typeOfEmployment = typeOfEmployment;
	}

	public WorkingStatus getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(WorkingStatus workingStatus) {
		this.workingStatus = workingStatus;
	}

	public void setWorkStatus(String status) {
		if (status.equals(WorkingStatus.PERMANENT_CONTRIBUTORY.getCode())) {
			setWorkingStatus(WorkingStatus.PERMANENT_CONTRIBUTORY);
			return;
		}
		if (status.equals(WorkingStatus.PERMANENT_NON_CONTRIBUTORY.getCode())) {
			setWorkingStatus(WorkingStatus.PERMANENT_NON_CONTRIBUTORY);
			return;
		}
		if (status.equals(WorkingStatus.PERMANENTLY_EXCLUDED.getCode())) {
			setWorkingStatus(WorkingStatus.PERMANENTLY_EXCLUDED);
			return;
		}
		if (status.equals(WorkingStatus.CASUAL_CONTRIBUTORY.getCode())) {
			setWorkingStatus(WorkingStatus.CASUAL_CONTRIBUTORY);
			return;
		}
		if (status.equals(WorkingStatus.CASUAL_NON_CONTRIBUTORY.getCode())) {
			setWorkingStatus(WorkingStatus.CASUAL_NON_CONTRIBUTORY);
			return;
		}
		if (status.equals(WorkingStatus.ACCUM_TO_DB_TRANSFER.getCode())) {
			setWorkingStatus(WorkingStatus.ACCUM_TO_DB_TRANSFER);
			return;
		}
		if (status.equals(WorkingStatus.DEATH.getCode())) {
			setWorkingStatus(WorkingStatus.DEATH);
			return;
		}
		if (status.equals(WorkingStatus.INACTIVE.getCode())) {
			setWorkingStatus(WorkingStatus.INACTIVE);
			return;
		}
		if (status.equals(WorkingStatus.INSOLUBLE.getCode())) {
			setWorkingStatus(WorkingStatus.INSOLUBLE);
			return;
		}
		if (status.equals(WorkingStatus.LOST_MEMBER.getCode())) {
			setWorkingStatus(WorkingStatus.LOST_MEMBER);
			return;
		}
		if (status.equals(WorkingStatus.UNCLAIMED_MONEY.getCode())) {
			setWorkingStatus(WorkingStatus.UNCLAIMED_MONEY);
			return;
		}
		if (status.equals(WorkingStatus.UNDER_65_NO_DEATH_CERTIFICATE.getCode())) {
			setWorkingStatus(WorkingStatus.UNDER_65_NO_DEATH_CERTIFICATE);
			return;
		}
	}

	public EmploymentDto clone() {
		return new EmploymentDto(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((employerCode == null) ? 0 : employerCode.hashCode());
		result = prime * result + ((typeOfEmployment == null) ? 0 : typeOfEmployment.hashCode());
		result = prime * result + ((workingStatus == null) ? 0 : workingStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmploymentDto other = (EmploymentDto) obj;
		if (employerCode == null) {
			if (other.employerCode != null)
				return false;
		} else if (!employerCode.equals(other.employerCode))
			return false;
		if (typeOfEmployment != other.typeOfEmployment)
			return false;
		if (workingStatus != other.workingStatus)
			return false;
		return true;
	}

}
