package au.test.timeline.model.dto;

import java.io.Serializable;

import au.test.timeline.TimeFramable;
import au.test.timeline.TimeFrame;
import au.test.timeline.TimeLine;

public class AccountDto extends TimeFrame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2835088173353722017L;

	private String accountID;
	private String memberGroup;
	private FundDto fund;
	private TimeLine<EmploymentDto> employments;
	private TimeLine<CategoryDto> categories;

	public AccountDto() {
	}
	
	public AccountDto(TimeFramable source) {
		super(source.getStartDate(), source.getEndDate());
	}
	
	public AccountDto(AccountDto source) {
		super(source.getStartDate(), source.getEndDate());
		this.accountID = source.getAccountID();
		this.memberGroup = source.getMemberGroup();
		this.fund = source.getFund();
		this.employments = source.getEmployments();
		this.categories = source.getCategories();
	}

	public String getMemberGroup() {
		return memberGroup;
	}

	public void setMemberGroup(String memberGroup) {
		this.memberGroup = memberGroup;
	}

	public FundDto getFund() {
		return fund;
	}

	public void setFund(FundDto fund) {
		this.fund = fund;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public TimeLine<EmploymentDto> getEmployments() {
		return employments;
	}

	public void setEmployments(TimeLine<EmploymentDto> employments) {
		this.employments = employments;
	}

	public TimeLine<CategoryDto> getCategories() {
		return categories;
	}

	public void setCategories(TimeLine<CategoryDto> categories) {
		this.categories = categories;
	}

	@Override
	public AccountDto clone() {
		return new AccountDto(this);
	}

}
