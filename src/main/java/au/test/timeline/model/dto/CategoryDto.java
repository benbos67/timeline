package au.test.timeline.model.dto;

import java.io.Serializable;

import au.test.timeline.TimeFramable;
import au.test.timeline.TimeFrame;
import au.test.timeline.enums.MemberStatus;

public class CategoryDto extends TimeFrame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2123966415454435597L;

	private String category;
	private MemberStatus status;

	public CategoryDto() {
	}
	
	public CategoryDto(TimeFramable source) {
		super(source.getStartDate(), source.getEndDate());
	}

	public CategoryDto(CategoryDto source) {
		super(source.getStartDate(), source.getEndDate());
		this.category = source.getCategory();
		this.status = source.getStatus();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public MemberStatus getStatus() {
		return status;
	}

	public void setStatus(MemberStatus status) {
		this.status = status;
	}

	@Override
	public CategoryDto clone() {
		return new CategoryDto(this);
	}

}
