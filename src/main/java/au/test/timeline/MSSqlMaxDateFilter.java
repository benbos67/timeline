package au.test.timeline;

import java.time.LocalDate;

import au.test.timeline.constants.MSSqlServer;

public class MSSqlMaxDateFilter {

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof LocalDate)) {
			return false;
		}
		LocalDate date = (LocalDate) obj;
		return date.isEqual(MSSqlServer.MS_SQL_MAX_DATE);
	}

}
